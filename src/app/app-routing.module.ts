/**
 * Tipo:
 * ARCHIVO DE CONFIGURACION DE RUTAS
 *
 * Descripción:
 * En este archivo se configura la ruta de cada elemento (componente de tipo pagina) con la cual sera
 * invocado desde su DOM, en dado caso necesite certificar rutas los puede hacer mediante CanActive
 * https://angular.io/api/router/CanActivate
 **/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
